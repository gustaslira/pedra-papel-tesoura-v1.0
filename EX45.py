from random import randint
from time import sleep

print('-=-' * 10, 'JOKENPO', '-=-' * 10)
print('''Bem-vindo ao Pedra, Papel e Tesoura!!!
[1] Pedra
[2] Papel
[3] Tesoura''')
comp = randint(1, 3)
choice = int(input('Selecione um dos números: '))

print('Jogador escolheu a opção {}'.format(choice))
sleep(2)
print('O computador está escolhendo...')
sleep(1)
print('Pronto!')
sleep(1)
print('Preparado? Vamos lá')
sleep(1)
print('-=-' * 10)

if choice == 1: #Jogador escolheu pedra
    print('\033[1;36mJO\033[m')
    sleep(1)
    print('\033[1;36mKEN\033[m')
    sleep(1)
    print('\033[1;36mPO\033[m')
    print('-=-' * 10)
    if comp == 1:
        print('EMPATE! O computador também escolheu Pedra')
    elif comp == 2:
        print('\033[1;31mVocê PERDEU! O Computador escolheu Papel\033[m')
    elif comp == 3:
        print('\033[1;32mVocê GANHOU! O Computador escolheu Tesoura\033[m')

if choice == 2: #jogador escolheu papel
    print('\033[1;36mJO\033[m')
    sleep(1)
    print('\033[1;36mKEN\033[m')
    sleep(1)
    print('\033[1;36mPO\033[m')
    print('-=-' * 10)
    if comp == 1:
        print('\033[1;32mVOCÊ GANHOU! O Computador escolheu Pedra/033[m')
    elif comp == 2:
        print('EMPATE! O computador também escolheu Papel\033[m')
    elif comp == 3:
        print('\033[1;31mVocê PERDEU O Computador escolheu Tesoura\033[m')

if choice == 3: #jogador escolheu tesoura
    print('\033[1;36mJO\033[m')
    sleep(1)
    print('\033[1;36mKEN\033[m')
    sleep(1)
    print('\033[1;36mPO\033[m')
    print('-=-' * 10)
    if comp == 1:
        print('\033[1;31mVOCÊ PERDEU! O Computador escolheu Pedra\033[m')
    elif comp == 2:
        print('\033[1;32mVocê GANHOU! O Computador escolheu Papel\033[m')
    elif comp == 3:
        print('EMPATE! O Computador também escolheu Tesoura')

else:
    print('NÚMERO INVÁLIDO SEU MONGOL! É DE 1 A 3!!!')